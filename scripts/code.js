// FULLPAGE --------------------- >
var anchors = ['about', 'petermac','kinetic','iSelect' ,'deakin-digital','mcri', 'trinity' , 'rmh', 'ccs','contact'];
var moveClasses = ['.browser','.featured','.laptop','.iphone','#about-section'];
var mobileBreakpoint = 1200;
var deviceWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)? true : false;



$(window).resize(function() {
  // This will execute whenever the window is resized
  deviceWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  if(deviceWidth <= mobileBreakpoint){
    $.fn.fullpage.setAutoScrolling(false);
  }else{
    $.fn.fullpage.setAutoScrolling(true);
  }
});




$(document).ready(function() {

    $( "#myMenu li a" ).hover(
      function() {
        var name = $(this).parent().attr('data-menuanchor');
        $( this ).parent().append( $( "<div class='anchor-name'>"+name+"</div>" ) );
      }, function() {
        $( this ).parent().find( "div:last" ).remove();
      }
    );

    //FULLPAGE
    $('#fullpage').fullpage({
        scrope : this,
        anchors: anchors,
        menu: '#myMenu',
        fitToSection:false,
        scrollingSpeed: 1200,
        autoScrolling: deviceWidth <= mobileBreakpoint? false: true,

        afterLoad: function(anchorLink, index){
            var loadedSection = $(this),
              color = $(this).attr('data-color');
            
            //$('body').css('background',color);
        },
        onLeave : function (index, nextIndex, direction){
          var nextSlideName = anchors[nextIndex - 1] + '-section';
            currentSlideName = anchors[index - 1] + '-section';

            
           if(!mobile) {

              //Animates elements in last and next index
              for(var i=0; i< moveClasses.length; i ++){
                var name = moveClasses[i];
                 $('#'+ currentSlideName).find(name).removeClass('move');
                 $('#'+ nextSlideName).find(name).addClass('move');
              }
              //Hack for about section
              if(currentSlideName == 'about' || nextIndex == 'about' ){
                $('#'+ currentSlideName).removeClass('move');
                $('#'+ nextSlideName).addClass('move');
              }
              
           } 
          
          
        }
    });

    
});



/*
PREVIOUS CODE FOR SCROLLING AND ANIMATING BACKGROUND
$(window).on("scroll touchmove", function() {
  console.log('hey wooo');
  if ($(document).scrollTop() >= $("#about").position().top) {
    $('body').css('background', $("#about").attr("data-color"));
    console.log('about');
  };

  if ($(document).scrollTop() > $("#localAgent").position().top) {
    $('body').css('background', $("#localAgent").attr("data-color"))
    console.log('localAgent');
  };

  if ($(document).scrollTop() >= $("#mcri").position().top) {
    $('body').css('background', $("#mcri").attr("data-color"))
    console.log('mcri');
  };
 
   if ($(document).scrollTop() >= $("#iSelect").position().top) {
    $('body').css('background', $("#iSelect").attr("data-color"))
     console.log('iSelect');
  };
  

});
*/