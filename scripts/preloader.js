$(document).ready(function() {
	//Array of all <img> in docuemnt
	var demoImgArray = [];
	$("img").each(
      function(index) {
         demoImgArray.push( $(this).attr("src") );
    });

	preload(demoImgArray);
 
 	//No Scroll Till loaded
 	$.fn.fullpage.setMouseWheelScrolling(false);
    $.fn.fullpage.setAllowScrolling(false);
    $.fn.fullpage.setKeyboardScrolling(false);
});

$(window).load(function() {
	//Scroll's back
	$.fn.fullpage.setMouseWheelScrolling(true);
	$.fn.fullpage.setAllowScrolling(true);
	$.fn.fullpage.setKeyboardScrolling(false);

	clearInterval(calcPercent);
    $( "#preloader .bar" ).fadeOut( "slow" );
    $("#myMenu").fadeIn("slow");
    $("#about-section #see").fadeIn("slow");
    loopSee();
});

function loopSee () {
	 $("#about-section #see" )
	    .animate({ bottom: "50px" }, 1000 , 'swing')
	    .animate({ bottom: "90px" }, 1000 , 'swing', function (){
	    	loopSee();
	    })
}

var percentage = 0,
	calcPercent;

function preload(imgArray) {
	var increment = Math.floor(100 / imgArray.length);
	$(imgArray).each(function() {
		$('<img>').attr("src", this).load(function() {
			percentage += increment;
		});
	});
	calcPercent = setInterval(function() {
		$('#preloader .bar').css('width',percentage+'%');

		
	},100);
}



